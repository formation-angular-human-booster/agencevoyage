<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Sejour;
use App\Entity\User;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SejourFixtures extends Fixture implements DependentFixtureInterface
{

    private $categoryRepository;

    public function __construct(CategoryRepository $categRepo)
    {
        $this->categoryRepository = $categRepo;
    }

    public function load(ObjectManager $manager)
    {
        $categSki = $this->categoryRepository->findOneBy(['nom'=> 'Montagne']);

        $sejour = new Sejour();
        $sejour->setCategory($categSki);
        $sejour->setDescription('Séjour à Valtho !');
        $sejour->setNbPersonne(3);
        $sejour->setTitre('Séjour ski');
        $sejour->setTypeLogement('Appartement');

        $manager->persist($sejour);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}