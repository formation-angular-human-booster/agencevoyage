<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Services\TestService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;
    private $testService;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TestService $testService){
        $this->encoder = $passwordEncoder;
        $this->testService = $testService;
    }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('usertest@test.com');
        $user->setFirstname('admin');
        $user->setLastname('test');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->encoder->encodePassword(
            $user,
            'testAdmin123'
        ));

        $user->setRedefinePassword(false);

        $this->testService->info('hello');

        $manager->persist($user);
        $manager->flush();
    }
}