<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CategoryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $categorys = ['Montagne', 'Mer', 'Plage'];
        foreach ($categorys as $categoryName) {
            $category = new Category();
            $category->setNom($categoryName);
            $manager->persist($category);
        }
        $manager->flush();

    }
}