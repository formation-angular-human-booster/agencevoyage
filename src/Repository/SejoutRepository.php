<?php

namespace App\Repository;

use App\Entity\Sejout;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sejout|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sejout|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sejout[]    findAll()
 * @method Sejout[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SejoutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sejout::class);
    }

    // /**
    //  * @return Sejout[] Returns an array of Sejout objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sejout
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
