<?php

namespace App\Repository;

use App\Entity\Activite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Activite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activite[]    findAll()
 * @method Activite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiviteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Activite::class);
    }

    public function search($string) {
        return $this->createQueryBuilder('act')
        ->where('act.nom LIKE :nom')
        ->setParameter('nom', '%'.$string.'%')->getQuery()->getResult();
    }

    public function findPaginate($nbElement, $pageAffiche) {
        return $this->createQueryBuilder('act')
            ->setMaxResults($nbElement)
            ->setFirstResult($nbElement * ($pageAffiche -1))
            ->getQuery()->getResult();
    }
    // /**
    //  * @return Activite[] Returns an array of Activite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneBySomeField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.nom LIKE :val')
            ->orWhere('a.id<10')
            ->setParameter('val', '%'.$value.'%')
            ->getQuery()
            ->getResult()
        ;
    }

}
