<?php
    namespace App\Services;
    use Psr\Log\LoggerInterface;

class TestService {
        private $logger;

       public function __construct(LoggerInterface $logger)
        {
            $this->logger = $logger;
        }

        public function info($message){
            $this->logger->info($message);
        }

        public function error($message) {
            $this->logger->error($message);
        }
    }
?>

