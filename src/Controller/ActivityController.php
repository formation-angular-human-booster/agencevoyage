<?php

namespace App\Controller;

use App\Entity\Activite;
use App\Form\ActiviteType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ActivityController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/activity", name="activity")
     * @Route("/activity/{page}", name="activity_paginate")
     */
    public function index($page = 1)
    {

        $nbElementParPage = 4;
        $activityRepository = $this->getDoctrine()->getRepository(Activite::class);
        $nbActivity=$activityRepository->count([]);
        $nbPage = $nbActivity/$nbElementParPage;
            $nbPage = ceil($nbPage);

         $resultatPagination = $activityRepository->findPaginate($nbElementParPage, $page);

         $this->logger->critical('OOOOOPPPPPPSSSS ERROR');


        return $this->render('activity/index.html.twig', [
            'nbPage'=> $nbPage,
            'activites'=> $resultatPagination
        ]);
    }

    /**
     * @Route("/activity-add", name="activity_add")
     */
    public function addActivity(Request $request, EntityManagerInterface $em) {
        $form = $this->createForm(ActiviteType::class, new Activite());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $form = $form->getData();
            $em->persist($form);
            $em->flush();
            return $this->redirectToRoute('activity');
        } else {
            return $this->render('activity/add.html.twig', [
                'activiteForm'=> $form->createView(),
                'errors'=> $form->getErrors()
            ]);
        }
    }

    /**
     * @Route("/activity-display/{activite}", name="activity_getone")
     */
    public function getOne(Activite $activite){

        return $this->render('activity/detail.html.twig', [
            'activite'=> $activite,

        ]);
    }

    /**
     * @Route("/activity-update/{activite}", name="activity_update")
     */
    public function editActivity(Activite $activite, Request $request,
                                 EntityManagerInterface $em)
    {
        $form = $this->createForm(ActiviteType::class, $activite);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('activity');
        } else {
            return $this->render('activity/edit.html.twig', [
                'activiteForm'=> $form->createView(),
                'errors'=> $form->getErrors(),
                'activite'=> $activite
            ]);
        }

    }


    /**
     * @Route("/activity-delete/{activite}", name="activity_delete")
     */
    public function delete(Activite $activite){
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($activite);
        $manager->flush();
        return $this->redirectToRoute('activity');
    }

    /**
     * @Route("/activity-search/{search}", name="activity_search")
     */
    public function activitySearch($search){
        $repository = $this->getDoctrine()->getRepository(Activite::class);
        $activitys = $repository->search($search);

        return $this->render('activity/search.html.twig', [
            'searchString' => $search,
            'activites'=> $activitys
        ]);
    }



    /**
     * @Route("/activity-name/{name}", name="activity_getone")

    public function getOneByName($name){
        $activite = $this->getDoctrine()->getRepository(Activite::class)->findBy(['nom'=> $name, 'description'=> 'test']);

        foreach ($activite as $act) {
            dump($act);
        }
        die();
        return $this->render('activity/detail.html.twig', [
        ]);
    } */
}
