<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Sejour;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {


        return $this->render('default/index.html.twig');
    }

    private function generateVersionNumber() : string
    {
        $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));

        return $now->format('is');
    }

}
