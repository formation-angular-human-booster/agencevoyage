<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OnlyLetter extends Constraint {
    public $message = 'Le nom de l\'activité ne peut contenir que des lettres';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}